import { Component, ViewChild,ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
declare const google;
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  constructor(
    public navCtrl: NavController,
    public geolocation: Geolocation
  ) {
  }
  @ViewChild('map') mapElement: ElementRef;
  map: any;
 ionViewDidLoad() {
     let posicao = { lat: -30.648139, lng: -10.717239 }
     this.map = new google.maps.Map(this.mapElement.nativeElement, {
         zoom: 40,
         center: posicao,
         mapTypeId: 'roadmap'
     });
     this.map.setCenter(posicao);
 }
  public local(){
  this.geolocation.getCurrentPosition().then((resp) => {
 console.log(resp.coords.latitude)
 console.log(resp.coords.longitude)
 let posicao = { lat: resp.coords.latitude, lng: resp.coords.longitude }
 this.map.setCenter(posicao)
 var marker = new google.maps.Marker({
  position: posicao,
  map: this.map
});
}).catch((error) => {
  console.log('Error getting location', error);
});
  }
}